public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Thanhlnd");
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);

        // diện tích hình tròn khi chưa truyền tham số
        System.out.println(circle1.getArea());

        // chu vi hình tròn khi chưa truyền tham số
        System.out.println(circle1.getCircumference());

        // diện tích hình tròn khi có tham số
        System.out.println(circle2.getArea());

        // chu vi hình tròn khi có tham số
        System.out.println(circle2.getCircumference());
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
    }
}

public class Circle {
    double radius = 1.0;

    // được gọi khi không truyền tham số
    public Circle() {
        super();
    }

    // được gọi khi truyền tham số
    public Circle(double radius) {
        this.radius = radius;
    }

    // setter getter
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    // hàm được gọi khi tính diện tích
    public double getArea() {
        return Math.PI * (radius * radius);
    }

    // hàm được gọi khi tính chu vi
    public double getCircumference() {
        return Math.PI * (2 * radius);
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }

}
